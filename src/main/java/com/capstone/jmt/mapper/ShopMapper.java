package com.capstone.jmt.mapper;


import com.capstone.jmt.data.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.access.method.P;
import sun.rmi.runtime.Log;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Jabito on 02/17/2017.
 */
public interface ShopMapper {


    LoginUser loadUserByEmail(@Param("email") String email);

    List<LoginUser> getAllUsers();

    List<Material> getAllMaterials();

    List<RefUser> getRefUsers();

    void addUser(@Param("newUser") LoginUser loginUser);

    LoginUser loadUserByEmpId(@Param("empId") String empId);

    void incrementId(@Param("id") int id);

    int getLastId(@Param("id") int id);

    void addMaterial(@Param("material") Material material);

    List<Supplier> getAllSuppliers();

    void addSupplier(@Param("supplier") Supplier supplier);


    HashMap getLastIdType(@Param("type") String type);

    void updateLastId(@Param("type") String type, @Param("approvalNo") int approvalNo);

    List<LoginUser> getUserBySearchText(@Param("searchText") String searchText);

    Department getAllDepartment(@Param("refRoleId") int refRoleid);

    void createPR(@Param("pr") PurchaseRequest purchaseRequest);

    void createPO(@Param("po") PurchaseOrder purchaseOrder);

    void createPurchaseMaterial(@Param("pm") PurchaseMaterials purchaseMaterials);

    List<PurchaseRequest> getAllPurchaseRequest(@Param("status") String status);

    PurchaseRequest getPurchaseRequestByRequestId(@Param("requestId") String requestId);

    List<PurchaseMaterials> getPurchaseMaterialsByRequestId(@Param("requestId") String requestId);

    String getRequestingDepartMentById(@Param("requestingDepartMent") int requestingDepartMent);

    String getPurchaseTypeById(@Param("id") int id);

    List<MaterialType> getAlMaterialTypes();

    List<Material> getMaterialsByType(@Param("id") int id,  @Param("offset") int offset);

    double getPriceOfMaterialByMatId(@Param("matId") String matId);

    double getTotalBudgetPerAccountCharge(@Param("id") int id);

    String getMaterialUnitByMatId(@Param("matId") String matId);

    String getMaterialDescByMatId(@Param("matId") String matId);

    void updatePurchaseRequestByRequestId(@Param("requestId") String requestId);

    void disapprovePRByRequestId(@Param("requestId") String requestId);

    String getAcccountChargeNameById(@Param("accountCharges") int accountCharge);

    String getMaterIdByName(@Param("materialName") String material_id);

    String getPositionByRoleId(@Param("refRoleUserId") int refRoleUserId);

    LoginUser getUserByEmpId(@Param("id") String id);

    List<Material> getAllMaterialsByJanuary();

    List<Material> getAllMaterialsByFeb();

    void updateUser(@Param("emp") LoginUser loginUser);

    LoginUser getUserByEmail(@Param("email") String email);

    List<Material> getAllMaterialsByMarch();

    List<Material> getAllMaterialsByApril();

    List<Material> getAllMaterialsByMay();

    List<Material> getAllMaterialsByJune();

    List<Material> getAllMaterialsByJuly();

    List<Material> getAllMaterialsByAugust();

    List<Material> getAllMaterialsBySept();

    List<Material> getAllMaterialsByOct();

    List<Material> getAllMaterialsByNov();

    List<Material> getAllMaterialsByDec();

    int getAllPendingEng();

    int getAllApprovedEng();

    int getAllPendingAcc();

    int getAllApprovedAcc();

    int getAllPendingPur();

    int getAllApprovedPur();

    Supplier getSupplierByName(@Param("suppName") String suppName);
}

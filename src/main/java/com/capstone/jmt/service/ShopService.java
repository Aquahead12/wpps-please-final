package com.capstone.jmt.service;

import com.capstone.jmt.data.*;
import com.capstone.jmt.mapper.OrderMapper;
import com.capstone.jmt.mapper.ShopMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Jabito on 15/02/2017.
 */
@Service("shopService")
public class ShopService {


    private static final Logger logger = LoggerFactory.getLogger(ShopService.class);


    @Autowired
    private ShopMapper shopMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    int ctr = 1;


    public LoginUser validateUser(LoginUser user) {
        logger.info("loadUserByUsername");
        System.out.println("SA SERVICE NA TO" + user.getEmail());
        System.out.println("SA SERVICE NA TO" + user.getPassword());

        LoginUser account = shopMapper.loadUserByEmail(user.getEmail());
        if (null != account) {
            System.out.println("SA SERVICE NA TOOOO: " + account.getPassword());
            if(user.getPassword().equals(account.getPassword())){
                return account;
            }else{
                return null;
            }
        } else
            return null;
    }

    public List<LoginUser> getAllUsers() {
        logger.info("getAllUsers");
        return shopMapper.getAllUsers();
    }

    public List<Material> getAllMaterials() {
        logger.info("getAllMaterials");
        return shopMapper.getAllMaterials();
    }

    public List<Supplier> getSupplier() {
        logger.info("getAllSupplier");
        return shopMapper.getAllSuppliers();
    }

    public void addMaterial(Material material) {
        logger.info("addMaterial");
        String matId = "";
        switch (material.getMaterialType()) {
            case 0:
                matId = generateID("paper");
                material.setMaterialId(matId);
                break;
            case 1:
                matId = generateID("envBox");
                material.setMaterialId(matId);
                break;
            case 2:
                matId = generateID("binder");
                material.setMaterialId(matId);
                break;
            case 3:
                matId = generateID("filing");
                material.setMaterialId(matId);
                break;
            case 4:
                matId = generateID("writing");
                material.setMaterialId(matId);
                break;
            case 5:
                matId = generateID("office storage");
                material.setMaterialId(matId);
                break;
            case 6:
                matId = generateID("furniture");
                material.setMaterialId(matId);
                break;
            case 7:
                matId = generateID("electrical");
                material.setMaterialId(matId);
                break;
            case 8:
                matId = generateID("AC tools");
                material.setMaterialId(matId);
                break;
            case 9:
                matId = generateID("AC consumables");
                material.setMaterialId(matId);
                break;


        }
        shopMapper.addMaterial(material);
    }

    public void addSupplier(Supplier supplier) {
        logger.info("addSupplier");
        String suppId = generateID("supp");
        supplier.setSupp_id(suppId);
        shopMapper.addSupplier(supplier);
    }

    public boolean addUser(LoginUser loginUser) {
        logger.info("adding New User");
        switch (loginUser.getRefRoleUserId()) {

            case 0:
                loginUser.setEmpId("PRM" + String.valueOf(shopMapper.getLastId(loginUser.getRefRoleUserId())));
                shopMapper.incrementId(loginUser.getRefRoleUserId());
                break;
            case 1:
                loginUser.setEmpId("CE" + String.valueOf(shopMapper.getLastId(loginUser.getRefRoleUserId())));
                shopMapper.incrementId(loginUser.getRefRoleUserId());
                break;
            case 2:
                loginUser.setEmpId("AM" + String.valueOf(shopMapper.getLastId(loginUser.getRefRoleUserId())));
                shopMapper.incrementId(loginUser.getRefRoleUserId());
                break;
            case 3:
                loginUser.setEmpId("PUM" + String.valueOf(shopMapper.getLastId(loginUser.getRefRoleUserId())));
                shopMapper.incrementId(loginUser.getRefRoleUserId());
                break;
            case 4:
                loginUser.setEmpId("SCM" + String.valueOf(shopMapper.getLastId(loginUser.getRefRoleUserId())));
                shopMapper.incrementId(loginUser.getRefRoleUserId());
                break;
            case 5:
                loginUser.setEmpId("EP" + String.valueOf(shopMapper.getLastId(loginUser.getRefRoleUserId())));
                shopMapper.incrementId(loginUser.getRefRoleUserId());
                break;
            case 6:
                loginUser.setEmpId("PP" + String.valueOf(shopMapper.getLastId(loginUser.getRefRoleUserId())));
                shopMapper.incrementId(loginUser.getRefRoleUserId());
                break;
            case 7:
                loginUser.setEmpId("AA" + String.valueOf(shopMapper.getLastId(loginUser.getRefRoleUserId())));
                shopMapper.incrementId(loginUser.getRefRoleUserId());
                break;
            case 8:
                loginUser.setEmpId("ADMIN" + String.valueOf(shopMapper.getLastId(loginUser.getRefRoleUserId())));
                shopMapper.incrementId(loginUser.getRefRoleUserId());
                break;

        }
        shopMapper.addUser(loginUser);
        return true;
    }

    public List<RefUser> getRefUsers() {
        return shopMapper.getRefUsers();
    }

    public String generateID(String type) {
        logger.info("generateID", type);
        //get last id
        HashMap lastid = shopMapper.getLastIdType(type);
        System.out.println("lastid:" + lastid);
        logger.info("lastid", lastid);
        int approvalNo = (int) lastid.get("no") + 1;
        logger.info("approvalNo", approvalNo);
        String prefix = (String) lastid.get("prefix");
        logger.info("prefix", prefix);
        int padding = 0;
        if (prefix != null) {
            padding += prefix.length();
        }
        padding += (int) lastid.get("length");
        shopMapper.updateLastId(type, approvalNo);
        String approvalNumString = String.valueOf(approvalNo);
        padding = padding - approvalNumString.length();
        logger.info("approvalNumString", approvalNumString);

        String result = "";
        if (padding > 0) {
            logger.info("padding", padding);
            result = String.format("%1$-" + padding + "s", prefix).replace(' ', '0') + approvalNumString;
            return result;
        } else {
            return approvalNumString;
        }
    }


    public List<LoginUser> getUserBySearchText(String searchText) {
        return shopMapper.getUserBySearchText("%"+searchText+"%");
    }

    public Department getAllDepartment(int refRoleid) {
        return shopMapper.getAllDepartment(refRoleid);
    }

    public String createPR(PurchaseRequest purchaseRequest) {
        System.out.println("NULL BA TO? " + purchaseRequest.getRequestingDepartMent());
        String requestId = generateID("PR");
        purchaseRequest.setRequestId(requestId);
        purchaseRequest.setStatus("PENDING");
        shopMapper.createPR(purchaseRequest);
        System.out.println("TAPOS NA MAG SAVE");

        savePurchaseMaterial(requestId, purchaseRequest);

        return requestId;
    }

    public void createPO(PurchaseOrder purchaseOrder) {

        String poId = generateID("PO");
        purchaseOrder.setPoId(poId);
        purchaseOrder.setRequestId(purchaseOrder.getRequestId());
        purchaseOrder.setStatus("PENDING");
        shopMapper.createPO(purchaseOrder);

    }

    public void savePurchaseMaterial(String requestId, PurchaseRequest purchaseRequest) {

        for (PurchaseMaterials purchaseMaterials : purchaseRequest.getPurchaseMaterials()) {
            purchaseMaterials.setRequest_id(requestId);
            purchaseMaterials.setTransaction_id(generateID("transaction"));
            purchaseMaterials.setCreated_by(purchaseRequest.getRequestingDepartMent());
            purchaseMaterials.setTotal_per_material(getTotalPerMaterial(purchaseMaterials.getMaterial_id(), purchaseMaterials.getQty()));
            purchaseMaterials.setTotal_budget(shopMapper.getTotalBudgetPerAccountCharge(Integer.parseInt(purchaseRequest.getAccountCharge())));
            purchaseMaterials.setMaterial_unit(shopMapper.getMaterialUnitByMatId(purchaseMaterials.getMaterial_id()));
            purchaseMaterials.setMaterial_desc(shopMapper.getMaterialDescByMatId(purchaseMaterials.getMaterial_id()));
            shopMapper.createPurchaseMaterial(purchaseMaterials);
        }
        System.out.println("SAVING PURCHASE MATERIALS");
        System.out.println("SAVING REQUEST ID" + requestId);
        System.out.println("================================");
    }

    public List<PurchaseRequest> getAllPurchaseRequest(String status) {
        return shopMapper.getAllPurchaseRequest(status);
    }

    public double getTotalPerMaterial(String matId, int qty) {

        double totalPerMaterial;
        double priceOfMaterial = shopMapper.getPriceOfMaterialByMatId(matId);

        totalPerMaterial = priceOfMaterial * qty;
        System.out.println("TOTAL PER MATERIAL" + totalPerMaterial);

        return totalPerMaterial;
    }

    public PurchaseRequest getPurchaseRequestByRequestId(String requestId) {
        return shopMapper.getPurchaseRequestByRequestId(requestId);
    }

    public List<PurchaseMaterials> getPurchaseMaterialsByRequestId(String requestId) {
        return shopMapper.getPurchaseMaterialsByRequestId(requestId);
    }

    public String getRequestingDepartMentById(int requestingDepartMent) {
        return shopMapper.getRequestingDepartMentById(requestingDepartMent);
    }

    public String getPurchaseTypeById(int id) {
        return shopMapper.getPurchaseTypeById(id);
    }

    public List<MaterialType> getAllMaterialTypes() {
        return shopMapper.getAlMaterialTypes();
    }

    public List<Material> getMaterialsByType(int id, int offset) {
        System.out.println("ID VAL" + id);
        return shopMapper.getMaterialsByType(id,offset);
    }

    public boolean getRolesPerActivity(int refRoleUserId) {
        if(refRoleUserId == 5 || refRoleUserId ==6 || refRoleUserId ==7)
            return true;
        else
            return false;
    }


    public void updatePurchaseRequestByRequestId(String requestId) {
        shopMapper.updatePurchaseRequestByRequestId(requestId);
    }

    public void disapprovePRByRequestId(String requestId) {
        shopMapper.disapprovePRByRequestId(requestId);
    }

    public String getAcccountChargeNameById(int accountCharge) {
        return shopMapper.getAcccountChargeNameById(accountCharge);
    }

    public String getMaterIdByName(String material_id) {
        return shopMapper.getMaterIdByName(material_id);
    }

    public String getPositionByRoleId(int refRoleUserId) {
        return shopMapper.getPositionByRoleId(refRoleUserId);
    }

    public LoginUser getUserByEmpId(String id) {
        return shopMapper.getUserByEmpId(id);
    }

    public List<Material> getAllMaterialsByJanuary() {
        return shopMapper.getAllMaterialsByJanuary();
    }

    public List<Material> getAllMaterialsByFeb() {
        return shopMapper.getAllMaterialsByFeb();
    }
    public List<Material> getAllMaterialsByMarch() {
        return shopMapper.getAllMaterialsByMarch();
    }

    public List<Material> getAllMaterialsByApril() {
        return shopMapper.getAllMaterialsByApril();
    }
    public List<Material> getAllMaterialsByMay() {
        return shopMapper.getAllMaterialsByMay();
    }
    public List<Material> getAllMaterialsByJune() {
        return shopMapper.getAllMaterialsByJune();
    }
    public List<Material> getAllMaterialsByJuly() {
        return shopMapper.getAllMaterialsByJuly();
    }
    public List<Material> getAllMaterialsByAugust() {
        return shopMapper.getAllMaterialsByAugust();
    }
    public List<Material> getAllMaterialsBySept() {
        return shopMapper.getAllMaterialsBySept();
    }
    public List<Material> getAllMaterialsByOct() {
        return shopMapper.getAllMaterialsByOct();
    }
    public List<Material> getAllMaterialsByNov() {
        return shopMapper.getAllMaterialsByNov();
    }
    public List<Material> getAllMaterialsByDec() {
        return shopMapper.getAllMaterialsByDec();
    }

    public void updateUser(LoginUser loginUser) {
        shopMapper.updateUser(loginUser);
    }

    public LoginUser getUserByEmail(String email) {
        return shopMapper.getUserByEmail(email);
    }

    public int getAllPendingEng() {
        return shopMapper.getAllPendingEng();
    }

    public int getAllApprovedEng() {
        return shopMapper.getAllApprovedEng();
    }

    public int getAllPendingAcc() {
        return shopMapper.getAllPendingAcc();
    }

    public int getAllApprovedAcc() {
        return shopMapper.getAllApprovedAcc();
    }
    public int getAllPendingPur() {
        return shopMapper.getAllPendingPur();
    }

    public int getAllApprovedPur() {
        return shopMapper.getAllApprovedPur();
    }

    public Supplier getSupplierByName(String suppName) {
        return shopMapper.getSupplierByName(suppName);
    }
}

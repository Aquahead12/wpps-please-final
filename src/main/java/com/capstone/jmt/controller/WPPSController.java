package com.capstone.jmt.controller;

import com.capstone.jmt.data.*;
import com.capstone.jmt.service.EmailService;
import com.capstone.jmt.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

/**
 * Created by macbookpro on 11/2/17.
 */
@Controller
@RequestMapping(value = "/")
@SessionAttributes("sessionUser")
public class WPPSController {

    @Autowired
    private ShopService shopService;

    @Autowired
    private EmailService emailService;

    private int increment = 0;


    @ModelAttribute("sessionUser")
    public LoginUser loginCurrentUser() {
        return new LoginUser();
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginCurrent(@RequestParam(value = "error", required = false) String error,
                               Model model) {
        if (null != error) {
            if (error.equals("1"))
                model.addAttribute("param.error", true);
            else if (error.equals("2"))
                model.addAttribute("param.logout", true);
        }
        model.addAttribute("user", new LoginUser());


        return "login";
    }


    @RequestMapping(value = "/loginUser", method = RequestMethod.POST)
    public String loginUser(LoginUser shop, Model model) {

        System.out.println("SHOP EMAIL: " + shop.getEmail());
        LoginUser user = shopService.validateUser(shop);
        if (null != user) {
            model.addAttribute("sessionUser", user);
            System.out.println("tama");
            return "redirect:/dashboard/";
        } else {
            System.out.println("MALI");
            return "redirect:/login/?error=" + "1";
        }
    }


    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String showDashboard(@RequestParam(value = "status", defaultValue = "ALL") String status, @ModelAttribute("sessionUser") LoginUser user, Model model) {

        List<PurchaseRequest> purchaseRequests = shopService.getAllPurchaseRequest(status);
        //List<Approval> approvals = shopService.getAll

        System.out.println("USER EMAIL: " + user.getEmail());
        if (user.getEmail() == null)
            return "redirect:/login";

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        model.addAttribute("engineeringPending", shopService.getAllPendingEng());
        model.addAttribute("engineeringApproved", shopService.getAllApprovedEng());
        model.addAttribute("accountingPending", shopService.getAllPendingAcc());
        model.addAttribute("accountingApproved", shopService.getAllApprovedAcc());
        model.addAttribute("purchasinggPending", shopService.getAllPendingPur());
        model.addAttribute("purchasingApproved", shopService.getAllApprovedPur());
        model.addAttribute("totalSize", purchaseRequests.size());
        model.addAttribute("adminRole", shopService.getRolesPerActivity(user.getRefRoleUserId()));

        return "dashboard";
    }

    @RequestMapping(value = "/request", method = RequestMethod.GET)
    public String showRequest(@ModelAttribute("sessionUser") LoginUser user, Model model) {

        List<LoginUser> users = shopService.getAllUsers();
        if(null == users){
            return "redirect:/login";
        }else {
            for (LoginUser user1 : users) {
                user1.setPosition(shopService.getPositionByRoleId(user1.getRefRoleUserId()));
            }
            model.addAttribute("users", users);
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            return "request";
        }
    }
    @RequestMapping(value = "/updatingUSER", method = RequestMethod.POST)
    public ResponseEntity<?>updatingUSER(@RequestBody UpateUserJSON upateUserJSON,@ModelAttribute("sessionUser") LoginUser user, Model model) {

        HashMap<String, Object> response = new HashMap<>();
        LoginUser loginUser = shopService.getUserByEmail(upateUserJSON.getEmail());
        loginUser.setFirstName(upateUserJSON.getFirstName());
        loginUser.setLastName(upateUserJSON.getLastName());
        loginUser.setContactNo(upateUserJSON.getContactNo());
        try{
            shopService.updateUser(loginUser);
            response.put("responseCode", 200);
            response.put("responseDesc", "Updated User SuccessFully");
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/approval", method = RequestMethod.GET)
    public String showApproval(@RequestParam(value = "status", defaultValue = "ALL") String status, @ModelAttribute("sessionUser") LoginUser user, Model model ) {

        List<PurchaseRequest> purchaseRequests = shopService.getAllPurchaseRequest(status);
        for(PurchaseRequest purchaseRequest: purchaseRequests) {
            List<PurchaseMaterials> materials =  shopService.getPurchaseMaterialsByRequestId(purchaseRequest.getRequestId());
            double totalCounter = 0.0;
            for(PurchaseMaterials purchaseMaterials: materials) {
                totalCounter = totalCounter + purchaseMaterials.getTotal_per_material();
            }
            purchaseRequest.setTotal(totalCounter);
            purchaseRequest.setRequestingDepartMent(shopService.getRequestingDepartMentById(Integer.parseInt(purchaseRequest.getRequestingDepartMent())));
        }

        if (purchaseRequests.isEmpty()) {
            System.out.println("NULL LIST");
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            model.addAttribute("roleUSer", shopService.getRolesPerActivity(user.getRefRoleUserId()));
            return "approval";
        } else {
            model.addAttribute("prList", purchaseRequests);
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            model.addAttribute("roleUSer", shopService.getRolesPerActivity(user.getRefRoleUserId()));
            return "approval";
        }
    }

    @RequestMapping(value = "/budgetHistory", method = RequestMethod.GET)
    public String budgetHistory(@RequestParam(value = "status", defaultValue = "ALL") String status, @ModelAttribute("sessionUser") LoginUser user, Model model) {

        List<PurchaseRequest> purchaseRequests = shopService.getAllPurchaseRequest(status);
        if (purchaseRequests.isEmpty()) {
            System.out.println("NULL LIST");
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            return "budgetHistory";
        } else {
            model.addAttribute("prList", purchaseRequests);
            model.addAttribute("januaryMaterials", shopService.getAllMaterialsByJanuary());
            model.addAttribute("februaryMaterials", shopService.getAllMaterialsByFeb());
            model.addAttribute("marchMaterials", shopService.getAllMaterialsByMarch());
            model.addAttribute("aprilMaterials", shopService.getAllMaterialsByApril());
            model.addAttribute("mayMaterials", shopService.getAllMaterialsByMay());
            model.addAttribute("juneMaterials", shopService.getAllMaterialsByJune());
            model.addAttribute("julyMaterials", shopService.getAllMaterialsByJuly());
            model.addAttribute("augustMaterials", shopService.getAllMaterialsByAugust());
            model.addAttribute("septMaterials", shopService.getAllMaterialsBySept());
            model.addAttribute("octMaterials", shopService.getAllMaterialsByOct());
            model.addAttribute("novMaterials", shopService.getAllMaterialsByNov());
            model.addAttribute("decMaterials", shopService.getAllMaterialsByDec());

            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            return "budgetHistory";
        }
    }
    @RequestMapping(value = "/budgetHistoryList", method = RequestMethod.GET)
    public String budgetHistoryList(@RequestParam(value = "status", defaultValue = "ALL") String status, @ModelAttribute("sessionUser") LoginUser user, Model model) {

        List<PurchaseRequest> purchaseRequests = shopService.getAllPurchaseRequest(status);
        if (purchaseRequests.isEmpty()) {
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            return "budgetHistoryList";
        } else {
            model.addAttribute("prList", purchaseRequests);
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            return "budgetHistoryList";
        }
    }

    @RequestMapping(value = "/procure", method = RequestMethod.GET)
    public String showProcure(@RequestParam(value = "status", defaultValue = "PENDING") String status, @ModelAttribute("sessionUser") LoginUser user, Model model) {

        List<PurchaseRequest> purchaseRequests = shopService.getAllPurchaseRequest(status);
        for(PurchaseRequest purchaseRequest: purchaseRequests) {
            List<PurchaseMaterials> materials =  shopService.getPurchaseMaterialsByRequestId(purchaseRequest.getRequestId());
            double totalCounter = 0.0;
            for(PurchaseMaterials purchaseMaterials: materials) {
                totalCounter = totalCounter + purchaseMaterials.getTotal_per_material();
            }
            purchaseRequest.setTotal(totalCounter);
            purchaseRequest.setRequestingDepartMent(shopService.getRequestingDepartMentById(Integer.parseInt(purchaseRequest.getRequestingDepartMent())));
        }

        if (purchaseRequests.isEmpty()) {
            System.out.println("NULL LIST");
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            return "procure";
        } else {
            model.addAttribute("prList", purchaseRequests);
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            return "procure";
        }
    }

    @RequestMapping(value = "/showPR", method = RequestMethod.GET)
    public String showPR(@ModelAttribute("sessionUser") LoginUser user, Model model, @RequestParam(value = "requestId") String requestId) {

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        PurchaseRequest purchaseRequest = shopService.getPurchaseRequestByRequestId(requestId);

        if (null == purchaseRequest) {
            System.out.println("NULL PURCHASE REQUEST");
            return "showPR";
        } else {
            purchaseRequest.setRequestingDepartMent(shopService.getRequestingDepartMentById(Integer.parseInt(purchaseRequest.getRequestingDepartMent())));
            purchaseRequest.setPurchaseType(shopService.getPurchaseTypeById(Integer.parseInt(purchaseRequest.getPurchaseType())));
            purchaseRequest.setAccountCharge(shopService.getAcccountChargeNameById(Integer.parseInt(purchaseRequest.getAccountCharge())));
            model.addAttribute("pr", purchaseRequest);
            model.addAttribute("purchaseMaterials", shopService.getPurchaseMaterialsByRequestId(requestId));
            return "showPR";
        }
    }

    @RequestMapping(value = "/showAccount", method = RequestMethod.GET)
    public String showAccount(@ModelAttribute("sessionUser") LoginUser user, Model model, @RequestParam(value = "id") String id) {

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        LoginUser loginUser = shopService.getUserByEmpId(id);

        if (null == loginUser) {
            System.out.println("NULL LOGIN USER");
            return "showAccount";
        } else {
            model.addAttribute("userDetail", loginUser);
            return "showAccount";
        }
    }

    @RequestMapping(value = "/approvePR", method = RequestMethod.POST)
    public ResponseEntity<?> approvePR(@RequestParam("requestId") String requestId) {

        HashMap<String, Object> response = new HashMap<>();
        try {
            shopService.updatePurchaseRequestByRequestId(requestId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/disapprovePR", method = RequestMethod.POST)
    public ResponseEntity<?> disapprovePR(@RequestParam("requestId") String requestId) {

        HashMap<String, Object> response = new HashMap<>();
        try {
            shopService.disapprovePRByRequestId(requestId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/showApproval", method = RequestMethod.GET)
    public String showApproval(@ModelAttribute("sessionUser") LoginUser user, Model model, @RequestParam(value = "requestId") String requestId) {

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        PurchaseRequest purchaseRequest = shopService.getPurchaseRequestByRequestId(requestId);

        if (null == purchaseRequest) {
            System.out.println("NULL PURCHASE REQUEST");
            return "showApproval";
        } else {
            purchaseRequest.setRequestingDepartMent(shopService.getRequestingDepartMentById(Integer.parseInt(purchaseRequest.getRequestingDepartMent())));
            purchaseRequest.setPurchaseType(shopService.getPurchaseTypeById(Integer.parseInt(purchaseRequest.getPurchaseType())));
            model.addAttribute("pr", purchaseRequest);
            model.addAttribute("purchaseMaterials", shopService.getPurchaseMaterialsByRequestId(requestId));
            return "showApproval";
        }
    }

    @RequestMapping(value = "/receive", method = RequestMethod.GET)
    public String showReceive(@ModelAttribute("sessionUser") LoginUser user, Model model, @RequestParam(value = "requestId") String requestId) {

        PurchaseRequest pr = shopService.getPurchaseRequestByRequestId(requestId);

        String accountCharge = shopService.getAcccountChargeNameById(Integer.parseInt(pr.getAccountCharge()));


        model.addAttribute("pr", pr);
        pr.setRequestingDepartMent(shopService.getRequestingDepartMentById(Integer.parseInt(pr.getRequestingDepartMent())));
        pr.setPurchaseType(shopService.getPurchaseTypeById(Integer.parseInt(pr.getPurchaseType())));
        pr.setAccountCharge(shopService.getAcccountChargeNameById(Integer.parseInt(pr.getAccountCharge())));
        model.addAttribute("accountCharge", accountCharge);

        model.addAttribute("purchaseMaterials", shopService.getPurchaseMaterialsByRequestId(requestId));
        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        return "receive";
    }

    @RequestMapping(value = "/manage", method = RequestMethod.GET)
    public String showManage(@ModelAttribute("sessionUser") LoginUser user, Model model) {

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        model.addAttribute("materials", shopService.getAllMaterials());
        return "manage";
    }

    @RequestMapping(value = "/addMaterial", method = RequestMethod.GET)
    public String showAddMaterial(@ModelAttribute("sessionUser") LoginUser user, Model model) {

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        model.addAttribute("materialTypes", shopService.getAllMaterialTypes());
        model.addAttribute("newMaterial", new Material());
        return "addMaterial";
    }

    @RequestMapping(value = "/addSupplier", method = RequestMethod.GET)
    public String showAddSupplier(@ModelAttribute("sessionUser") LoginUser user, Model model) {

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        model.addAttribute("materialList", shopService.getAllMaterialTypes());
        model.addAttribute("newSupplier", new Supplier());
        return "addSupplier";
    }

    @RequestMapping(value = "/addNewSupplier", method = RequestMethod.POST)
    public String addMaterial(@ModelAttribute("sessionUser") LoginUser user, @Valid Supplier supplier, Model model) {

        System.out.println(" MATERIAL TYPE SELECTED : " + supplier.getMaterial_type());
        shopService.addSupplier(supplier);
        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        return "redirect:/report";
    }

    @RequestMapping(value = "/addNewMaterial", method = RequestMethod.POST)
    public String addMaterial(@ModelAttribute("sessionUser") LoginUser user, @Valid Material material, Model model) {

        System.out.println("MATERIAL ENTITY: " + material);
        System.out.println("MATERIAL ENTITY: " + material.getMaterialDesc());
        System.out.println("MATERIAL ENTITY: " + material.getPrice());
        System.out.println("MATERIAL ENTITY: " + material.getQty());
        System.out.println("MATERIAL ENTITY: " + material.getMaterialUnit());
        shopService.addMaterial(material);
        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        return "redirect:/dashboard";
    }

    @RequestMapping(value = "/report", method = RequestMethod.GET)
    public String showReport(@ModelAttribute("sessionUser") LoginUser user, Model model) {

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        model.addAttribute("suppliers", shopService.getSupplier());
        return "report";
    }

    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String showSettings(@RequestParam(value = "status", defaultValue = "APPROVED") String status,@ModelAttribute("sessionUser") LoginUser user, Model model) {

        List<PurchaseRequest> purchaseRequests = shopService.getAllPurchaseRequest(status);
        for(PurchaseRequest purchaseRequest: purchaseRequests) {
            List<PurchaseMaterials> materials =  shopService.getPurchaseMaterialsByRequestId(purchaseRequest.getRequestId());
            double totalCounter = 0.0;
            for(PurchaseMaterials purchaseMaterials: materials) {
                totalCounter = totalCounter + purchaseMaterials.getTotal_per_material();
            }
            purchaseRequest.setTotal(totalCounter);
            purchaseRequest.setRequestingDepartMent(shopService.getRequestingDepartMentById(Integer.parseInt(purchaseRequest.getRequestingDepartMent())));
        }

        if (purchaseRequests.isEmpty()) {
            System.out.println("NULL LIST");
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            return "settings";
        } else {
            model.addAttribute("prList", purchaseRequests);
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            return "settings";
        }
    }

    @RequestMapping(value = "/supplier", method = RequestMethod.GET)
    public String showSupplier(@ModelAttribute("sessionUser") LoginUser user, Model model) {

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        model.addAttribute("suppliers", shopService.getSupplier());
        return "supplier";
    }

    @RequestMapping(value = "/adduser", method = RequestMethod.GET)
    public String showAdduser(@ModelAttribute("sessionUser") LoginUser user, Model model) {

        //Setting value to USER
        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        model.addAttribute("newUser", new LoginUser());
        model.addAttribute("typeOfUSer", shopService.getRefUsers());
        return "adduser";
    }

    @RequestMapping(value = "/addNewUser", method = RequestMethod.POST)
    public String addNewUser(@ModelAttribute("sessionUser") LoginUser user, @Valid LoginUser loginUser, BindingResult bindingResult, Model model) {

        //Trying to save a New USER

        shopService.addUser(loginUser);
        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        return "redirect:/request";
    }

    @RequestMapping(value = "/budget", method = RequestMethod.GET)
    public String showBudget(@ModelAttribute("sessionUser") LoginUser user, Model model) {

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        return "budget";
    }

    @RequestMapping(value = "/invoice", method = RequestMethod.GET)
    public String showSample(@ModelAttribute("sessionUser") LoginUser user, Model model) {

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        return "invoice";
    }

    @RequestMapping(value = "/createPR", method = RequestMethod.GET)
    public String createPR(@ModelAttribute("sessionUser") LoginUser user, Model model) {

        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        model.addAttribute("materialTypes", shopService.getAllMaterialTypes());
        model.addAttribute("materials", shopService.getAllMaterials());
        model.addAttribute("requestDepartment", shopService.getAllDepartment(user.getRefRoleUserId()));
        return "createPR";
    }

    @RequestMapping(value = "/createPO", method = RequestMethod.GET)
    public String createPO(@RequestParam(value = "requestId", required = true) String requestId, @ModelAttribute("sessionUser") LoginUser user, Model model) {

        PurchaseRequest purchaseRequest = shopService.getPurchaseRequestByRequestId(requestId);
        model.addAttribute("email", user.getEmail());
        model.addAttribute("user", user);
        model.addAttribute("pr", purchaseRequest);
        model.addAttribute("supplier", shopService.getSupplier());
        model.addAttribute("purchaseMaterials", shopService.getPurchaseMaterialsByRequestId(requestId));


        return "createPO";
    }

    @RequestMapping(value = "/savingPR", method = RequestMethod.POST)
    public ResponseEntity<?> savingPR(@RequestBody PurchaseRequest matName) {

        System.out.println("DATE SUBMITTED: " + matName.getDatePickerSubmitted());
        System.out.println("DATE DUE: " + matName.getDatePickerDue());
        HashMap<String, Object> response = new HashMap<>();
        if (null == matName) {
            response.put("null object", HttpStatus.NOT_FOUND);
        } else {
            String generatedKey = shopService.createPR(matName);
            response.put("generatedKey", generatedKey);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/savingPO", method = RequestMethod.POST)
    public ResponseEntity<?> savingPO(@RequestBody PurchaseOrder purchaseOrder) {

        System.out.println("DATE SUBMITTED: " + purchaseOrder.getCreatedOn());
//        System.out.println("DATE DUE: " + matName.getDatePickerDue());
        HashMap<String, Object> response = new HashMap<>();
        if (null == purchaseOrder) {
            response.put("null object", HttpStatus.NOT_FOUND);
        } else {
            shopService.createPO(purchaseOrder);
            response.put("response Code", HttpStatus.OK);
            response.put("response desc", "Purchase Order Created Successfully!");
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/getMaterialsById", method = RequestMethod.GET)
    public ResponseEntity<?> getMaterialsById(@RequestParam(value = "id", defaultValue = "0") int id,
                                              @RequestParam(value = "offset", defaultValue = "0") int offset) {

        HashMap<String, Object> response = new HashMap<>();
        List<Material> materials = shopService.getMaterialsByType(id, offset);
        if (materials.isEmpty()) {
            response.put("null object", HttpStatus.NOT_FOUND);
        } else {
            response.put("materialList", materials);
            response.put("size", materials.size());
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/supplierByName", method = RequestMethod.GET)
    public ResponseEntity<?> supplierByName(@RequestParam(value = "name") String suppName) {

        HashMap<String, Object> response = new HashMap<>();
        Supplier supplier = shopService.getSupplierByName(suppName);
        if (null == supplier) {
            response.put("null object", HttpStatus.NOT_FOUND);
        } else {
            response.put("supp", supplier);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
    public ResponseEntity<?> sendEmail(@RequestParam(value = "email") String email) {

        HashMap<String, Object> response = new HashMap<>();
        try{
            emailService.testSend(email);
            response.put("responseCode", 200);
            response.put("responseDesc", HttpStatus.OK);
        }catch (Exception e) {
            response.put("responseCode", 404);
            response.put("responseDesc", HttpStatus.NOT_FOUND);
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/getSummaryDetails", method = RequestMethod.GET)
    public String getSummaryDetails(@ModelAttribute("sessionUser") LoginUser user, @RequestParam(value = "requestId") String requestId, Model model) {

        PurchaseRequest purchaseRequest = shopService.getPurchaseRequestByRequestId(requestId);
        if (null == purchaseRequest) {
            System.out.println("NULL");
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            return "getSummaryDetails";
        } else {
            purchaseRequest.setRequestingDepartMent(shopService.getRequestingDepartMentById(Integer.parseInt(purchaseRequest.getRequestingDepartMent())));
            purchaseRequest.setPurchaseType(shopService.getPurchaseTypeById(Integer.parseInt(purchaseRequest.getPurchaseType())));
            purchaseRequest.setAccountCharge(shopService.getAcccountChargeNameById(Integer.parseInt(purchaseRequest.getAccountCharge())));
            model.addAttribute("email", user.getEmail());
            model.addAttribute("user", user);
            model.addAttribute("pr", purchaseRequest);
            model.addAttribute("purchaseMaterials", shopService.getPurchaseMaterialsByRequestId(requestId));
            return "getSummaryDetails";
        }
    }

    @RequestMapping(value = "/searchUser", method = RequestMethod.POST)
    public ResponseEntity<?> searchUser(@RequestParam(value = "searchText") String searchText) {
        System.out.println("SEARCH TEXT: " + searchText);
        HashMap<String, Object> response = new HashMap<>();
        if(searchText.equals("")){
            List<LoginUser> loginUsers = shopService.getAllUsers();
            for (LoginUser user2 : loginUsers) {
                user2.setPosition(shopService.getPositionByRoleId(user2.getRefRoleUserId()));
            }
            response.put("responseCode", 200);
            response.put("users", loginUsers);
        }else {

            List<LoginUser> users = shopService.getUserBySearchText(searchText);
            if (null == users) {
                response.put("responseCode", 404);
                response.put("responseDesc", HttpStatus.NOT_FOUND);
            } else {
                for (LoginUser user1 : users) {
                    user1.setPosition(shopService.getPositionByRoleId(user1.getRefRoleUserId()));
                }
                response.put("responseCode", 200);
                response.put("users", users);
            }
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}

package com.capstone.jmt.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by macbookpro on 8/5/18.
 */
public class PurchaseOrder {

    @JsonProperty("poId")
    private String poId;

    @JsonProperty("requestId")
    private String requestId;

    @JsonProperty("createdOn")
    private String createdOn;

    @JsonProperty("createdBy")
    private String createdBy;

    @JsonProperty("updatedOn")
    private String updatedOn;

    @JsonProperty("updatedBy")
    private String updatedBy;

    @JsonProperty("remarks")
    private String remarks;

    @JsonProperty("supplierName")
    private String supplierName;

    @JsonProperty("supplierAddress")
    private String supplierAddress;

    @JsonProperty("supplierContactNo")
    private String supplierContactNo;

    @JsonProperty("attn")
    private String attn;

    @JsonProperty("terms")
    private String terms;

    @JsonProperty("status")
    private String status;

    public PurchaseOrder(String poId, String requestId, String createdOn, String createdBy, String updatedOn, String updatedBy, String remarks, String supplierName, String supplierAddress, String supplierContactNo, String attn, String terms, String status) {
        this.poId = poId;
        this.requestId = requestId;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
        this.remarks = remarks;
        this.supplierName = supplierName;
        this.supplierAddress = supplierAddress;
        this.supplierContactNo = supplierContactNo;
        this.attn = attn;
        this.terms = terms;
        this.status = status;
    }

    public String getPoId() {
        return poId;
    }

    public void setPoId(String poId) {
        this.poId = poId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierAddress() {
        return supplierAddress;
    }

    public void setSupplierAddress(String supplierAddress) {
        this.supplierAddress = supplierAddress;
    }

    public String getSupplierContactNo() {
        return supplierContactNo;
    }

    public void setSupplierContactNo(String supplierContactNo) {
        this.supplierContactNo = supplierContactNo;
    }

    public String getAttn() {
        return attn;
    }

    public void setAttn(String attn) {
        this.attn = attn;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

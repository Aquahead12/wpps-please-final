package com.capstone.jmt.data;

import java.util.Date;

public class Supplier {

    private String supp_id;
    private String supp_name;
    private String supp_email;
    private String supp_contact_no;
    private String supp_contact_person;
    private Date created_on;
    private Date updated_on;
    private String created_by;
    private String update_by;
    private String supp_address;
    private int material_type;


    public int getMaterial_type() {
        return material_type;
    }

    public void setMaterial_type(int material_type) {
        this.material_type = material_type;
    }

    public String getSupp_address() {
        return supp_address;
    }

    public void setSupp_address(String supp_address) {
        this.supp_address = supp_address;
    }

    public void setSupp_id(String supp_id) {
        this.supp_id = supp_id;
    }

    public String getSupp_id() {
        return supp_id;
    }

    public void setSupp_name(String supp_name) {
        this.supp_name = supp_name;
    }

    public String getSupp_name() {
        return supp_name;
    }

    public void setSupp_email(String supp_email) {
        this.supp_email = supp_email;
    }

    public String getSupp_email() {
        return supp_email;
    }

    public void setSupp_contact_no(String supp_contact_no) {
        this.supp_contact_no = supp_contact_no;
    }

    public String getSupp_contact_no() {
        return supp_contact_no;
    }

    public String getSupp_contact_person() {
        return supp_contact_person;
    }

    public void setSupp_contact_person(String supp_contact_person) {
        this.supp_contact_person = supp_contact_person;
    }

    public Date getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }

    public Date getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(Date updated_on) {
        this.updated_on = updated_on;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdate_by() {
        return update_by;
    }

    public void setUpdate_by(String update_by) {
        this.update_by = update_by;
    }
}



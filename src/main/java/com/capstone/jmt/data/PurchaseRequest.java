package com.capstone.jmt.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by macbookpro on 2/21/18.
 */
public class PurchaseRequest{

    @JsonProperty("requestId")
    private String requestId;
    @JsonProperty("purchaseMaterials")
    ArrayList<PurchaseMaterials> purchaseMaterials;
    @JsonProperty("requestingDepartMent")
    String requestingDepartMent;
    @JsonProperty("purpose")
    String purpose;
    @JsonProperty("datePickerSubmitted")
    private String datePickerSubmitted;
    @JsonProperty("datePickerDue")
    private String datePickerDue;
    @JsonProperty("accountCharge")
    String accountCharge;
    @JsonProperty("purchaseType")
    String purchaseType;
    @JsonProperty("status")
    String status;
    private double total;

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ArrayList<PurchaseMaterials> getPurchaseMaterials() {
        return purchaseMaterials;
    }

    public void setPurchaseMaterials(ArrayList<PurchaseMaterials> purchaseMaterials) {
        this.purchaseMaterials = purchaseMaterials;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

    public String getDatePickerSubmitted() {
        return datePickerSubmitted;
    }

    public void setDatePickerSubmitted(String datePickerSubmitted) {
        this.datePickerSubmitted = datePickerSubmitted;
    }

    public String getDatePickerDue() {
        return datePickerDue;
    }

    public void setDatePickerDue(String datePickerDue) {
        this.datePickerDue = datePickerDue;
    }

    public String getAccountCharge() {
        return accountCharge;
    }

    public void setAccountCharge(String accountCharge) {
        this.accountCharge = accountCharge;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getRequestingDepartMent() {
        return requestingDepartMent;
    }

    public void setRequestingDepartMent(String requestingDepartMent) {
        this.requestingDepartMent = requestingDepartMent;
    }


}

package com.capstone.jmt.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by macbookpro on 4/14/18.
 */
public class Department {

    @JsonProperty("id")
    int id;
    @JsonProperty("dept_name")
    String dept_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }
}

package com.capstone.jmt.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by macbookpro on 4/15/18.
 */
public class PurchaseMaterials {

    @JsonProperty("transaction_id")
    private String transaction_id;
    @JsonProperty("request_id")
    private String request_id;
    @JsonProperty("material_id")
    private String material_id;
    @JsonProperty("created_on")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date created_on;
    @JsonProperty("created_by")
    private String created_by;
    @JsonProperty("remarks")
    private String remarks;
    @JsonProperty("qty")
    private int qty;
    @JsonProperty("total_per_material")
    private double total_per_material;
    @JsonProperty("total_budget")
    private double total_budget;
    @JsonProperty("material_unit")
    private String material_unit;
    @JsonProperty("material_desc")
    private String material_desc;

    public double getTotal_budget() {
        return total_budget;
    }

    public void setTotal_budget(double total_budget) {
        this.total_budget = total_budget;
    }

    public String getMaterial_unit() {
        return material_unit;
    }

    public void setMaterial_unit(String material_unit) {
        this.material_unit = material_unit;
    }

    public String getMaterial_desc() {
        return material_desc;
    }

    public void setMaterial_desc(String material_desc) {
        this.material_desc = material_desc;
    }

    public double getTotal_per_material() {
        return total_per_material;
    }

    public void setTotal_per_material(double total_per_material) {
        this.total_per_material = total_per_material;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(String material_id) {
        this.material_id = material_id;
    }

    public Date getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}

package com.capstone.jmt.data;

/**
 * Created by macbookpro on 4/26/18.
 */
public class MaterialType {

    int id;
    String material_type;
    double total_budget;

    public double getTotal_budget() {
        return total_budget;
    }

    public void setTotal_budget(double total_budget) {
        this.total_budget = total_budget;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaterial_type() {
        return material_type;
    }

    public void setMaterial_type(String material_type) {
        this.material_type = material_type;
    }
}

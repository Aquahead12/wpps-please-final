var randomScalingFactor = function () {
    return Math.round(Math.random() * 25000 + 3)
};

var lineChartData = {
    labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        },
        {
            label: "My Second dataset",
            fillColor: "rgba(128,130,228,0.6)",
            strokeColor: "rgba(128,130,228,1)",
            pointColor: "rgba(128,130,228,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(128,130,228,1)",
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        }
    ]

}
var pieData = [
    {
        value: 3,
        color:"#30a5ff",
        highlight: "#62b9fb",
        label: "Approved"
    },
    {
        value: 10,
        color: "#ffb53e",
        highlight: "#fac878",
        label: "Pending"
    }

];
var pieData1 = [
    {
        value: 4,
        color:"#30a5ff",
        highlight: "#62b9fb",
        label: "Approved"
    },
    {
        value: 9,
        color: "#ffb53e",
        highlight: "#fac878",
        label: "Pending"
    }

];
var pieData2 = [
    {
        value: 5,
        color:"#30a5ff",
        highlight: "#62b9fb",
        label: "Approved"
    },
    {
        value: 8,
        color: "#ffb53e",
        highlight: "#fac878",
        label: "Pending"
    }

];
		
